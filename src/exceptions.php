<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <dkrizan@synopsis.cz>
 * Date: 08.06.20 20:19
 */

class CsvNotFoundException extends \Exception {

}

class FileNotReadableException extends \Exception {

}

class EmptyCSVFileException extends \Exception {

}

class TagNotFoundException extends \Exception {

}