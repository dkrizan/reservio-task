<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <dkrizan@synopsis.cz>
 * Date: 08.06.20 19:42
 */

class Tag {

    /** @var mixed */
    protected $value;

    /** @var int */
    protected $id;

    /** @var string */
    protected $name;

    /** @var Tag */
    protected $parent;

    /** @var Tag[] */
    protected $children = [];

    /**
     * Tag constructor.
     * @param int $id
     * @param $name
     * @param $value
     * @param Tag|null $parent
     */
    public function __construct(int $id, $name, $value, Tag $parent = null) {
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;
        $this->parent = $parent;
    }

    /**
     * Sets tag's parent
     * @param Tag $parent
     */
    public function setParent(Tag $parent) {
        $this->parent = $parent;
    }

    /**
     * Adds child
     * @param Tag $tag
     */
    public function addChild(Tag $tag) {
        $this->children[] = $tag;
        $tag->setParent($this);
    }

    /**
     * Sums all children tag's value recursively
     * @param int $sum
     * @return int
     */
    public function sumChildren() {
        $iterator = function (Tag $tag, &$sum = 0) use (&$iterator) {
            foreach ($tag->getChildren() as $child) {
                $sum += $child->getValue();
                $iterator($child, $sum);
            }
        };
        $iterator($this, $sum);
        return (int)$sum;
    }

    /**
     * Removes child tag
     * @param Tag $child
     */
    public function removeChild(Tag $child) {
        unset($this->children[array_search($child, $this->children)]);
    }

    /**
     * Children tags
     * @return Tag[]
     */
    public function getChildren(): array {
        return $this->children;
    }

    /**
     * Returns tag value
     * @return mixed
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * Returns tag name
     * @return string
     */
    public function getName(): string {
        return $this->name;
    }

    /**
     * Returns tag id
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * Return tag parent
     * @return Tag
     */
    public function getParent(): Tag {
        return $this->parent;
    }

    /**
     * Converts tag to array
     * @return array
     */
    public function toArray() : array {
        return [
            'id' => $this->id,
            'parent_id' => $this->parent instanceof Tag ? $this->parent->getId() : null,
            'identifier' => $this->getName(),
            'value' => $this->getValue()
        ];
    }

}