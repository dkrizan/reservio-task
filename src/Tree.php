<?php
/**
 * Created by PhpStorm.
 * @author Daniel Krizan <dkrizan@synopsis.cz>
 * Date: 08.06.20 19:41
 */

include_once 'Tag.php';
include_once 'exceptions.php';

class Tree {

    const VALUE_KEY = "value",
        ID_KEY = "id",
        NAME_KEY = "identifier",
        PARENT_KEY = "parent_id";

    /** @var Tag */
    protected $root;

    /** @var Tag[] */
    protected $tags = [];

    /**
     * Tree constructor.
     * @param string $file
     * @throws CsvNotFoundException
     * @throws EmptyCSVFileException
     * @throws FileNotReadableException
     */
    public function __construct(string $file) {
        $this->buildFromCSV($file);
    }

    /**
     * Builds tree from CSV file
     * @param string $file
     * @param string $delimiter
     * @throws CsvNotFoundException
     * @throws FileNotReadableException
     * @throws EmptyCSVFileException
     */
    public function buildFromCSV(string $file, string $delimiter = ",") {
        if (!file_exists($file)) {
            throw new CsvNotFoundException();
        }
        if (!is_readable($file)) {
            throw new FileNotReadableException();
        }

        if (($reader = fopen($file, 'r')) !== false) {
            if (($header = fgetcsv($reader, 5000, $delimiter)) === false) {
                throw new EmptyCSVFileException();
            }
            $tagRelations = [];
            while (($data = fgetcsv($reader, 5000, $delimiter)) !== false)  {
                $row = array_combine($header, $data);
                $this->tags[$row[self::ID_KEY]] = $tag = new Tag(
                    $row[self::ID_KEY],
                    $row[self::NAME_KEY],
                    $row[self::VALUE_KEY]
                );
                if (empty($row[self::PARENT_KEY])) {
                    $this->root = $tag;
                }
                if (!isset($tagRelations[$row[self::PARENT_KEY]])) {
                    $tagRelations[$row[self::PARENT_KEY]] = [];
                }
                $tagRelations[$row[self::PARENT_KEY]][] = $row[self::ID_KEY];
            }

            foreach ($tagRelations as $parent => $children) {
                foreach ($children as $id) {
                    if (isset($this->tags[$parent])) {
                        $this->tags[$parent]->addChild($this->tags[$id]);
                    }
                }
            }
            fclose($reader);
        }
    }

    /**
     * Move subtree
     * @param Tag $child
     * @param Tag $parent
     */
    public function moveSubTree(Tag $child, Tag $parent) {
        $oldParent = $child->getParent();
        $child->setParent($parent);
        $parent->addChild($child);
        $oldParent->removeChild($child);
    }

    /**
     * Returns tag by id
     * @param int $id
     * @return Tag
     * @throws TagNotFoundException
     */
    public function getTag(int $id) {
        if (!isset($this->tags[$id])) {
            throw new TagNotFoundException();
        }
        return $this->tags[$id];
    }

    /**
     * Returns root Tag
     * @return Tag
     */
    public function getRoot(): Tag {
        return $this->root;
    }

    /**
     * Returns all tags
     * @return Tag[]
     */
    public function getTags(): array {
        return $this->tags;
    }

    /**
     * Export tree to CSV
     */
    public function exportToCsv() {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="export.csv";');
        $file = fopen('php://output', 'w');
        fputcsv($file, [self::ID_KEY, self::PARENT_KEY, self::NAME_KEY, self::VALUE_KEY]);
        foreach ($this->getTags() as $tag) {
            fputcsv($file, $tag->toArray(), ',');
        }
        die;
    }
}