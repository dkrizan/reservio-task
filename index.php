<?php
include_once 'src/Tree.php';

if (isset($_POST['upload'])) {
    $file = $_FILES['csv']['tmp_name'];
    try {
        $tree = new Tree($file);
        session_start();
        $_SESSION['tree'] = $tree;
        header("Location: tree.php");
    } catch (CsvNotFoundException $e) {
        $error = "CSV file not found!";
    } catch (EmptyCSVFileException $e) {
        $error = "CSV file does not contains data!";
    } catch (FileNotReadableException $e) {
        $error = "Unable to read CSV file!";
    }
}
?>

<html>
    <head>
        <title>Reservio</title>
        <link type="text/css" href="public/css/style.css" rel="stylesheet" />
    </head>
    <body>
        <div class="header">
            <h3>Reservio - Task</h3>
        </div>
        <?php
        if (isset($error)) {
            echo "<div class='flash-error'>'<h3>" . $error . "</h3></div>";
        }
        ?>
        <div class="body">
            <div class="content">
                <p>Firstly upload your CSV file.</p>
                <form method="post" enctype="multipart/form-data">
                    <label>CSV File:</label>
                    <input type="file" name="csv" />

                    <input type="submit" name="upload" value="Upload"/>
                </form>
            </div>
        </div>
    </body>
</html>