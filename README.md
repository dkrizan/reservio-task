Reservio - Task
------
Simple application to modify tree structured data saved in CSV.


### Installing

A step by step how to get a development env running.

Install NPM dependencies

```
npm install
```

build LESS files

```
gulp less
```

and run the app.

```
docker-compose up
```

App is available at http://localhost:1234/