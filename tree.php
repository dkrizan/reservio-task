<?php
include_once "src/Tree.php";
session_start();
$tree = $_SESSION['tree'];
if (!$tree instanceof Tree) {
    header("Location: index.php");
}

if ($_POST['move']) {
    try {
        $tag = $tree->getTag($_POST['tag']);
    } catch (TagNotFoundException $e) {
        $error = "Moving tag not found (id : " .$_POST['tag'] . ")";
    }
    try {
        $parent = $tree->getTag($_POST['parent']);
    } catch (TagNotFoundException $e) {
        $error = "Parent tag not found (id : " .$_POST['parent'] . ")";
    }
    $tree->moveSubTree($tag, $parent);
}

if ($_POST['sum']) {
    try {
        $sumSubmitted = true;
        $sum = $tree->getTag($_POST['tag'])->sumChildren();
    } catch (TagNotFoundException $e) {
        $error = "Tag not found (id : " .$_POST['tag'] . ")";
    }
}

if ($_POST['export']) {
    $tree->exportToCsv();
}

function renderTree(Tag $tag) {
    ob_start();
    treeHtmlBuilder($tag);
    return ob_get_clean();
}

function treeHtmlBuilder(Tag $tag) {
?>
    <li>
        <div class="item"><?= sprintf("[%s] %s (%s)", $tag->getId(), $tag->getName(), $tag->getValue())?></div>
    <?php
    if (count($tag->getChildren())) {
    ?>
        <ul>
            <?php
                foreach ($tag->getChildren() as $child) {
                    treeHtmlBuilder($child);
                }
            ?>
        </ul>
    <?php
    }
    ?>
    </li>
<?php
}
?>

<html>
<head>
    <title>Tree | Reservio</title>
    <link type="text/css" href="public/css/style.css" rel="stylesheet" />
</head>
<body>
<div class="header">
    <h3>Reservio - Task</h3>
</div>
<?php if (isset($error)) { ?>
    <div class="flash-error">
        <?= $error ?>
    </div>
<?php } ?>
<div class="body">
    <div class="content wide">
        <a href="index.php">back</a>
        <div class="tools">
            <div class="main">
                <form method="post" class="box">
                    <div class="title">Move subtree</div>
                    <input type="text" name="tag" placeholder="Tag id" />
                    <input type="text" name="parent" placeholder="New parent id" />
                    <input type="submit" name="move" class="btn btn-primary" value="Move" />
                </form>
                <form method="post" class="box">
                    <div class="title">Sum subtree</div>
                    <input type="text" name="tag" placeholder="Tag id" />
                    <input type="submit" name="sum" class="btn btn-primary" value="Sum" />
                    <?php echo((isset($sumSubmitted)) ? ('<div class="result">Result: ' . $sum . '</div>') : '');?>
                </form>
            </div>
            <form method="post" class="other">
                <input type="submit" class="btn btn-export" name="export" value="Export to CSV" />
            </form>
        </div>
        <h2>Tags</h2>
        <ul class="tree">
            <?php echo renderTree($tree->getRoot()); ?>
        </ul>
    </div>
</div>
</body>
</html>
